# include <stdio.h>

int main() {

    double radius, Area;

    printf("Enter a value for the radius: ");
    scanf("%lf", &radius);

    Area = 3.14 * radius * radius;

    printf("Area of the circle is %.3f\n", Area);

    return 0;
}