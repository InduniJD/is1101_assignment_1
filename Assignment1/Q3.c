#include <stdio.h>

int main(){

    int a, b;

    printf("Enter a value for a = ");
    scanf("%d", &a);

    printf("Enter a value for b = ");
    scanf("%d", &b);

    int temp = a;
    a = b;
    b = temp;

    printf("After swaping a = %d , b = %d\n", a, b);

    return 0;

}