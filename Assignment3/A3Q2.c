#include <stdio.h>

int main(){
    int num, p;

    printf("Enter a number: ");
    scanf("%d", &num);

    for (p = 2; p <= num/2; p++)
    {
        if (num%p == 0)
        {
            printf("%d is a composite number.", num);
            break;
        }
    }
    if (p == num/2 +1)
    {
        printf("%d is a prime number", num);
    }
    
return 0;
}