#include <stdio.h>

int main(){

    char l;

    printf("Enter any character: ");
    scanf("%c", &l);

    if (l=='a' || l=='e' || l=='i' || l=='o' || l=='u' 
     || l=='A' || l=='E' || l=='I' || l=='O' || l=='U' )
    {
        printf("%c is a Vowel.", l);
    }
    else if ((l >= 'a' && l <= 'z') || (l >= 'A' && l <= 'Z'))
    {
        printf("%c is a Consonant.", l);
    }
    else
    {
        printf("This is not in alphabet.");
    }

return 0;    
}